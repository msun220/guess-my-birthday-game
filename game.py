from random import randint

name = input("Hi! What is your name? ")
num_of_tries = 5
current_try = 0
response = "no"
start_month = 1
end_month = 12
start_year = 1924
end_year = 2004

lst_month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

month = randint(start_month, end_month)
year = randint(start_year, end_year)

while (current_try != num_of_tries) and (response != "yes"):

    current_try += 1
    print("Guess", current_try, ":", name, "were you born in", lst_month[month-1], "/", year, "?")

    resp = input("yes, later, or earlier? ")
    response = resp.lower()

    if (response != "yes") and (current_try == num_of_tries):
        print("I have other things to do. Good bye.")

    elif response == "yes":
        print("I knew it!")

    elif response == "later":
        print("Drat! Lemme try again!")
        start_year = year
        new_year = randint(start_year, end_year)
        if new_year == start_year:
            month = randint(month, end_month)
        else:
            month = randint(start_month, end_month)
        year = new_year

    elif response == "earlier":
        print("Drat! Lemme try again!")
        end_year = year
        new_year = randint(start_year, end_year)
        if new_year == year:
            month = randint(start_month, month)
        else:
            month = randint(start_month, end_month)
        year = new_year
